1.12.2-0.1.1
-----

* Fixed a bug that would allow players to use the alarm clock on a bed while sneaking, desyncing the alarm texture
* Modified default config values
* Now with real textures, courtesy of XavierDD!


1.12.2-0.0.2
-----

* Added Config file support with options for
    * Alarm Clocks only work on the player's own bed (default: yes)
    * Number of "minecraft hours" (x1000 ticks) that the player must wait between using the Alarm Clock (default: 0.0, but with a hardcoded minimum of 20 ticks to prevent spamming)
    * Maximum number of "day steps" that the alarm clock will allow the player to skip (default: 4)
    * Customizable message displayed when a player tries to sleep for too long
    * An option NOT to disable alarm clocks on servers, since some people play modpacks not with random obnoxious trolls, but with their friends - i.e. a *select group* of obnoxious trolls ;). (default: false)
      
* Bug Fixes/Tweaks:
    * Fixed the Foot-of-the-bed bug (Issue #7)
    * Informational messages are now sent as status messages instead of chat messages. 


1.12.2-0.0.1
-----

* Initial Release