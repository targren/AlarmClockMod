package com.targren.alarmclock.Common.Items;

import com.targren.alarmclock.Core.Config;
import com.targren.alarmclock.Core.ItemBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockBed;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraft.world.WorldProvider;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import static net.minecraft.block.BlockBed.PART;
import static net.minecraft.block.BlockHorizontal.FACING;

public class ItemAlarmClock extends ItemBase {

    int current = 0;

    public ItemAlarmClock(String name, CreativeTabs tab) {
        super(name, tab);
        setMaxDamage(0);
        setMaxStackSize(1);
    }


    public boolean isLocalGame() {
        if (FMLCommonHandler.instance() != null) {
            if (FMLCommonHandler.instance().getMinecraftServerInstance() != null) {
                return (!FMLCommonHandler.instance().getMinecraftServerInstance().isDedicatedServer());
            }
        }
        //If we've gotten to this point, then something along the way was null, and that won't happen if
        //we're in a local game in a position for this to be called!
        return false;
    }

    @SideOnly(Side.CLIENT)
    public void initModel() {
        repaintItem();
    }

    @SideOnly(Side.CLIENT)
    public void repaintItem() {
        ModelResourceLocation sunriseModel = new ModelResourceLocation(getRegistryName() + "_sunrise");
        ModelResourceLocation noonModel = new ModelResourceLocation(getRegistryName() + "_noon");
        ModelResourceLocation midnightModel = new ModelResourceLocation(getRegistryName() + "_midnight");
        ModelResourceLocation sunsetModel = new ModelResourceLocation(getRegistryName() + "_sunset");
        ModelResourceLocation disabledModel = new ModelResourceLocation(getRegistryName() + "_disabled");

        ModelBakery.registerItemVariants(this, sunriseModel, noonModel, sunsetModel, midnightModel, disabledModel);

        ModelLoader.setCustomMeshDefinition(this, new ItemMeshDefinition() {
            @Override
            public ModelResourceLocation getModelLocation(ItemStack stack) {
                ModelResourceLocation model = null;
                int alarm = getAlarmTime(stack);
                if (!(Config.allowOnMP || isLocalGame())) {
                    alarm = -1;
                }

                switch (alarm) {
                    case 0: {
                        model = sunriseModel;
                        break;
                    }
                    case 1: {
                        model = noonModel;
                        break;
                    }
                    case 2: {
                        model = sunsetModel;
                        break;
                    }
                    case 3: {
                        model = midnightModel;
                        break;
                    }
                    case -1: {
                        model = disabledModel;
                    }
                    default: {
                        model = ((Config.allowOnMP || isLocalGame()) ? sunriseModel : disabledModel);
                        break;
                    }
                }
                return model;
            }
        });
    }

    //Item functionality
    private int getAlarmTime(ItemStack stack) {
        NBTTagCompound NBT = stack.getTagCompound();
        if (NBT == null) {
            NBT = new NBTTagCompound();
        }

        if (NBT.hasKey("AlarmTime")) {
            return NBT.getShort("AlarmTime");
        } else {
            NBT.setShort("AlarmTime", (short) 0);
            stack.setTagCompound(NBT);
            return 0;
        }
    }

    private int IncrementAlarm(ItemStack stack) {
        int current = getAlarmTime(stack);
        if (current == 3) {
            current = 0;
        } else {
            ++current;
        }

        NBTTagCompound NBT = stack.getTagCompound();
        NBT.setShort("AlarmTime", (short) current);
        return current;
    }

    //Force the alarm type to -1
    private int DisableClock(ItemStack stack) {
        NBTTagCompound NBT = stack.getTagCompound();
        NBT.setShort("AlarmTime", (short) -1);
        return current;
    }

    //Set the alarm time by right-clicking holding the alarm clock
    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);
        current = getAlarmTime(stack);
        if (player.isSneaking()) {
            if (Config.allowOnMP || isLocalGame()) {
                current = IncrementAlarm(stack);
                //Increment the alarm time
                if (world.isRemote) {
                    repaintItem();
                    player.sendStatusMessage(new TextComponentTranslation("string.alarmclock.alarmset").appendSibling(getAlarmString(getAlarmTime(stack))), false);
                }
                return new ActionResult<>(EnumActionResult.SUCCESS, stack);
            } else {
                DisableClock(stack);
                if (world.isRemote) {
                    player.sendMessage(new TextComponentTranslation("string.alarmclock.noservers"));
                    repaintItem();
                }
                return new ActionResult<>(EnumActionResult.FAIL, stack);
            }
        } else {
            return new ActionResult<>(EnumActionResult.PASS, stack);
        }
    }

    @Override
    public EnumActionResult onItemUseFirst(EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);
        EnumActionResult SUCCESS = new ActionResult<>(EnumActionResult.SUCCESS, stack).getType();
        EnumActionResult FAIL = new ActionResult<>(EnumActionResult.FAIL, stack).getType();
        EnumActionResult PASS = new ActionResult<>(EnumActionResult.PASS, stack).getType();

        if (!world.isRemote) {
            if (player.isSneaking()){
                return PASS;
            }
            if (player.getCooldownTracker().getCooldown(this, 0f) > 0f) {
                return FAIL;
            }

            if (world.getBlockState(pos).getBlock().isBed(world.getBlockState(pos), world, pos, player)) {
                if (Config.ownBedOnly) {
                    BlockPos BedLoc = player.getBedLocation();
                    IBlockState bs = world.getBlockState(pos);

                    if (BedLoc == null){
                        player.sendStatusMessage(new TextComponentTranslation("string.alarmclock.notyourbed"), false);
                        return FAIL;
                    } else if (!pos.equals(BedLoc)) {
                        //Someday, I pray I can come back to this part and do this as a less clusterfucky kluge
                        //I hate myself right now
                        /*
                        //The clicked bed block isn't the same as  the spawn location, but beds are two blocks long
                        //Since I can't work out how to determine the bed parts yet, just let it go through if the clicked bed block
                        //Is adjacent to the returned bed location
                        BlockPos.MutableBlockPos offset = new BlockPos.MutableBlockPos((pos.subtract(BedLoc)));

                        //Hooray for convenient simplifications that save calls to Math.sqrt

                        if (Math.abs(offset.getX()+offset.getY()+offset.getZ()) != 1){
                            player.sendStatusMessage(new TextComponentTranslation("string.alarmclock.notyourbed"), false);
                            return FAIL;
                        }*/

                        //Today is that day. The above has been kept for inspirational purposes.
                        if (bs.getValue(PART) == BlockBed.EnumPartType.FOOT){
                            if (!(pos.offset(bs.getValue(FACING)).equals(BedLoc))) {
                                player.sendStatusMessage(new TextComponentTranslation("string.alarmclock.notyourbed"), false);
                                return FAIL;
                            }
                        }
                    }
                }

                if (world.provider.canSleepAt(player, pos) == WorldProvider.WorldSleepResult.ALLOW) {
                    EntityPlayer.SleepResult PSR = player.trySleep(pos);
                    long wakeup = calculateWakeup(getAlarmTime(stack), world);

                    if (player.isPlayerSleeping()) { //If the player isn't sleeping (i.e. it's daytime) then trying to wake them up makes Bad Things(TM) happen
                        player.wakeUpPlayer(true, false, false); //This fixes Night->Night sleeping bug
                    }

                    if (!timeInRange(wakeup, world)) {
                        player.sendStatusMessage(new TextComponentString(Config.tooLongMsg), false);
                        return FAIL;
                    } else if (PSR == EntityPlayer.SleepResult.OK || PSR == EntityPlayer.SleepResult.NOT_POSSIBLE_NOW) {
                        player.sendStatusMessage(new TextComponentTranslation("string.alarmclock.sleeptil").appendSibling(getAlarmString(getAlarmTime(stack))), false);
                        //[Todo] Is it possible to play the sleep animation anyway?
                        //Set a the alarm clock cooldown to the greater of 20 ticks (~1 rw second) or a value calculated from Config.alarmClockCooldown. The minimum cooldown is to prevent spamming from destabilizing things
                        int tickCooldown = Math.max(20, Math.round(Config.alarmClockCooldown * 1000));
                        player.getCooldownTracker().setCooldown(this, tickCooldown);
                        world.setWorldTime(wakeup);
                        return SUCCESS;
                    }
                }
                return PASS;

            }
        }
        return PASS;
    }

    private boolean timeInRange(long wakeupTime, World world) {
        int maxtime = Config.maxSleepTime;
        if (maxtime == 4) {
            return true;
        }
        long currentWorldtime = world.getWorldTime();
        return (maxtime > ((wakeupTime - currentWorldtime) / 6000));
    }


    private long calculateWakeup(int alarm, World world) {
        long currentWorldTime = world.getWorldTime();
        long currentDayTime = currentWorldTime % 24000;
        long currentDayStart = currentWorldTime - currentDayTime;
        long wakeupTime = 0;
        long target = alarm * 6000;

        //If we're already past the wakeup time
        wakeupTime = ((target > currentDayTime) ? currentDayStart + target : currentDayStart + 24000 + target);

        return wakeupTime;
    }

    private TextComponentTranslation getAlarmString(int alarm) {
        String key = "";
        switch (alarm) {
            case 0: {
                key = "string.alarmclock.dawn";
                break;
            }
            case 1: {
                key = "string.alarmclock.noon";
                break;
            }
            case 2: {
                key = "string.alarmclock.dusk";
                break;
            }
            case 3: {
                key = "string.alarmclock.midnight";
                break;
            }
            default: {
                key = "string.alarmclock.herebedragons"; //Should not happen
                break;
            }
        }
        return new TextComponentTranslation(key);
    }

}