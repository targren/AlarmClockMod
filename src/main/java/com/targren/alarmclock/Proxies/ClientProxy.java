package com.targren.alarmclock.Proxies;

import com.targren.alarmclock.Core.ItemBase;
import com.targren.alarmclock.Core.ModItems;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;


@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientProxy extends CommonProxy {
    @Override
    public void preInit(FMLPreInitializationEvent e) {
        super.preInit(e);
    }

    @Override
    public void postInit(FMLPostInitializationEvent e){
        super.postInit(e);
        //Re-render item models here
        ModItems.initModels();
    }

    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event) {
        ModItems.initModels();
    }

    public static void repaintItem(ItemBase i){

        if (i == null){ System.out.println("WTF did you do?"); } else {
            i.repaintItem();
        }
    }

}
