package com.targren.alarmclock.Proxies;

import com.targren.alarmclock.Core.Config;
import com.targren.alarmclock.Core.ItemBase;
import com.targren.alarmclock.Core.ModItems;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.io.File;
import java.util.Map;


@Mod.EventBusSubscriber
public class CommonProxy {

    public static Configuration config;

    public void preInit(FMLPreInitializationEvent e) {
        File cfgdir = e.getModConfigurationDirectory();
        config = new Configuration(new File(cfgdir.getPath(), "alarmclock.cfg"));
        Config.ReadConfig();

    }

    public void init(FMLInitializationEvent e) {
    }

    public void postInit(FMLPostInitializationEvent e) {
        if (config.hasChanged()){
            config.save();
        }
    }

    public void registerItemRenderer(Item item, int meta, String id) {/*NOOP*/}


    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        ModItems.initItemsList();

        for(Map.Entry<String, ItemBase> i: ModItems.Items.entrySet()){
            event.getRegistry().register(i.getValue());
        }
    }

    public static void repaintItem(ItemBase i){ /*NOOP*/}
}
