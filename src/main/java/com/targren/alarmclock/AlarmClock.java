package com.targren.alarmclock;

import com.targren.alarmclock.Proxies.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import org.apache.logging.log4j.Logger;


@Mod(modid = AlarmClock.modId, name = AlarmClock.name, version = AlarmClock.version, acceptedMinecraftVersions = "[1.12,1.12.2]")
public class AlarmClock {

    public static final String modId = "alarmclock";
    public static final String name = "Alarm Clock";
    public static final String version = "1.12-0.0.2";

    public static final SimpleNetworkWrapper net = NetworkRegistry.INSTANCE.newSimpleChannel("AlarmClock");
    public static Logger logger;

    @SidedProxy(clientSide = "com.targren.alarmclock.Proxies.ClientProxy", serverSide = "com.targren.alarmclock.Proxies.ServerProxy")
    public static CommonProxy proxy;

    @Mod.Instance(modId)
    public static AlarmClock instance;


    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }

}
