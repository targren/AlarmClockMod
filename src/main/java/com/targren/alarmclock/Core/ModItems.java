package com.targren.alarmclock.Core;

import com.targren.alarmclock.Common.Items.ItemAlarmClock;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.HashMap;
import java.util.Map;

public class ModItems {

    public static HashMap<String, ItemBase> Items = new HashMap<String,ItemBase>();

    public static void initItemsList(){
        Items.put("alarmclock", new ItemAlarmClock("alarmclock", CreativeTabs.TOOLS));
    }

    @SideOnly(Side.CLIENT)
    public static void initModels(){
        for(Map.Entry<String, ItemBase> i: ModItems.Items.entrySet()){
            i.getValue().initModel();
        }
    }
}
