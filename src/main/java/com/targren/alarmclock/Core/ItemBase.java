package com.targren.alarmclock.Core;

import com.targren.alarmclock.AlarmClock;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBase extends Item {

        protected String name;
        protected CreativeTabs tab = null;

        public ItemBase(){/*NOOP*/}

        public ItemBase(String name) {
           this.name = name;
           setUnlocalizedName(name);
           setRegistryName(name);
        }

        public ItemBase(String name, CreativeTabs tab) {
            this.name = name;
            this.tab = tab;
            setUnlocalizedName(name);
            setRegistryName(name);
            super.setCreativeTab(tab);
        }

        @SideOnly(Side.CLIENT)
        public void initModel(){
            System.out.println("[TARGREN] Calling ItemBase#initModel");
            ModelLoader.setCustomModelResourceLocation(
                    this,0,
                    new ModelResourceLocation(getRegistryName().toString()));
        }

        public void repaintItem(){
            AlarmClock.proxy.repaintItem(this);
        }

        @Override
        public ItemBase setCreativeTab(CreativeTabs tab){
            super.setCreativeTab(CreativeTabs.TOOLS);
            return this;
        }

        protected String getTextureName() {
            return this.getRegistryName().toString();
        }



    }

