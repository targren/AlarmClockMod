package com.targren.alarmclock.Core;

import com.targren.alarmclock.AlarmClock;
import com.targren.alarmclock.Proxies.CommonProxy;
import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.Level;


public class Config {

    public static final String CNFCAT = "general";
    public static final String MPCAT  = "multiplayer";

    public static boolean ownBedOnly         = true;  //If true, the player can only use the alarm clock on his own bed.
    public static boolean allowOnMP          = false; //If true, don't disable the alarm clock on Multiplayer Servers.
    public static int     maxSleepTime       = 3;     //The maximum number of "steps" (Midnight->Dawn = 1 step, Midnight->Noon = 2 steps, etc.) that the alarm clock can be used for. Midnight->Midnight = 4
    public static float   alarmClockCooldown = 1f;     //How many "minecraft hours" the player must wait before using the alarm clock again.
    public static String  tooLongMsg         = "If you tried to sleep for that long, you'd wet the bed!";


    public static void ReadConfig(){
        Configuration config = CommonProxy.config;
        try{
            config.load();
            initConfig(config);
        } catch (Exception ex){
            AlarmClock.logger.log(Level.ERROR, "Could not load alarmclock.cfg file", ex);
        } finally {
            if (config.hasChanged()){
                config.save();
            }
        }
    }

    private static void initConfig(Configuration config){
        config.addCustomCategoryComment(CNFCAT,"AlarmClock Mod Configuration");
        ownBedOnly = config.getBoolean("ownBedOnly", CNFCAT, ownBedOnly, "If true, the player can only use the alarm clock on the bed that they last slept in");
        maxSleepTime = config.getInt("maxSleepTime", CNFCAT, maxSleepTime, 0,4,"The maximum number of \"steps\" the Alarm Clock can progress time. Midnight->Noon = 2 steps, Midnight->Midnight = 4 steps. 1 step = 6000 \"minecraft seconds\"");
        alarmClockCooldown = config.getFloat("alarmClockCooldown", CNFCAT, alarmClockCooldown,0f,100f, "How many \"minecraft hours\" must pass before the player can use an alarm clock again (1 minecraft hour = 1000 ticks)\nThere is always a 20-tick cooldown to prevent spamming");
        tooLongMsg = config.getString("tooLongMsg", CNFCAT, tooLongMsg, "The message displayed to the player if they try to use the alarm clock for more than <maxSleepTime> steps");
        allowOnMP = config.getBoolean("allowonMP", MPCAT, allowOnMP, "If true, don't disable the alarm clock on Multiplayer servers (very experimental. Enable at your own risk!)");
    }

}
