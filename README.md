**Sneak + Right Click with the clock in your hand**: Set the time to wake up. The Alarm clock has 4 (actually 5. More on that below) settings, and the (horrible, yes, I know) icon textures will change to reflect the current setting.

* Sunrise (Left)
* Noon (Up)
* Sunset (Right)
* Midnight (Down)

**Right Click a bed with a set alarm clock to sleep**: The alarm clock will allow you to use your bed regardless of time of day, subject to certain conditions:

* *The bed is your current spawn bed.* (configurable) That is, the bed must be the last one you slept in, and you must have done so before the clock will work.
* *Except for the time of day, you must be able to sleep.* Being in a dimension that doesn't allow sleeping, having monster aggro, etc.. will stop the alarm clock from working.
* *You are playing a single-player game.* (configurable) As a former server-admin, I can imagine that letting any player with access to a bit of gold and redstone mess with the world time would just be a little too troll-friendly. If the mod detects it is running on a server, the texture will change to show that the tool is disabled (the "fifth state" mentioned above).

*The Alarm Clock doesn't reset the time.* The mod calculates the game time for the next time the selected time of day comes about, it does not reset the time to the first day. That is, if you use a clock set to Sunrise just after Midnight on Day 63, you will "wake up" at Sunrise of Day 63. If you set it to Noon and use it at sunset of Day 63, you will "wake up" at Noon of Day 64.  This means that it will not interfere with other mods such as Scaled Health or Astral Sorcery which may depend on the in-game calendar.

